
# ----------------------------------------------------------------------
# Terraform S3 backend 
# ----------------------------------------------------------------------

terraform {
    backend "s3" {
        bucket = "current-terraform-state"
        # The key is provided in the gitlab-ci.yml file, at the terraform init step.
        region = "eu-west-1"
        dynamodb_table = "current-terraform-state"
        encrypt = true
    }
    required_providers {
        aws = {
        source  = "hashicorp/aws"
        version = "~> 4.16"
        }
    }

  required_version = ">= 0.14.0"
}

provider "aws" {
  region  = var.aws_region
}
# ----------------------------------------------------------------------
# Variables
# ----------------------------------------------------------------------
variable environment {
  description = "The environment to create things in."
  # validation {
  #   condition = length(var.environment) <= 15
  #   error_message = "The environment variable (e.g. the Gitlab branch name) must be 15 characters or less."
  # }
}

variable aws_region {
  description = "The AWS region to create things in."
}

variable "account_id" {
  description = "The AWS account ID."
}

aws_region = "eu-west-1"
account_id = "129012441001"


# ----------------------------------------------------------------------
# API Gateway Trust Policy
# ----------------------------------------------------------------------
data "aws_iam_policy_document" "apigateway_trust_policy" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

# ----------------------------------------------------------------------
# API Gateway Lambda execution policy
# ----------------------------------------------------------------------
data "aws_iam_policy_document" "apigateway_lambda_policy" {
  statement {
    effect    = "Allow"
    actions   = ["lambda:InvokeFunction"]
    resources = values(data.aws_cloudformation_export.api_lambda_arn_cfn_exports)[*].value
  }
}
# ----------------------------------------------------------------------
# create ECR
# ----------------------------------------------------------------------

resource "aws_ecr_repository" "ecr_repository_api" {
  name = "ecr-repository-api"
  force_delete = true
}

# ----------------------------------------------------------------------
# Docker
# ----------------------------------------------------------------------
resource "null_resource" "ecr_image_api" {
 triggers = {
   python_file = md5(file("${path.module}/app/main.py"))
   docker_file = md5(file("${path.module}/app/Dockerfile"))
 }  
  provisioner "local-exec" {
   command = <<EOF
           aws ecr get-login-password --region ${var.aws_region} | docker login --username AWS --password-stdin ${var.account_id}.dkr.ecr.${var.aws_region}.amazonaws.com
           cd ${path.module}/app
           docker build -t ${aws_ecr_repository.ecr_repository_api.repository_url}:${var.ecr_image_api_tag} .
           docker push ${aws_ecr_repository.ecr_repository_api.repository_url}:${var.ecr_image_api_tag}
       EOF
 }
}


# ----------------------------------------------------------------------
# Image Id
# ----------------------------------------------------------------------
data aws_ecr_image lambda_image_api {
 depends_on = [
   null_resource.ecr_image_wapi
 ]
 repository_name = "ecr-repository-api"
 image_tag       = var.ecr_image_api_tag
}

# ----------------------------------------------------------------------
# lambda role
# ----------------------------------------------------------------------
resource "aws_iam_role" "iam_lambda_role_api" {
  name = "api-lambda-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

# ----------------------------------------------------------------------
# lambda policy
# ----------------------------------------------------------------------
resource "aws_iam_role_policy" "iam_lambda_policy_api" {
  name = "api-lambda-policy"
  role = aws_iam_role.iam_lambda_role_api.id
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        
        "Action": [
          "sqs:*"
        ],
        "Effect": "Allow",
        "Resource": "*"
      },
      {
        "Action": [
          "S3:PutObject",
          "S3:GetObject",
          "S3:ListBucket"
        ],
        "Effect": "Allow",
        "Resource": "*"
      },
      {
        "Action": [
          "logs:*"
        ],
        "Effect": "Allow",
        "Resource": "*"
      },
      {
        
        "Action": [
          "execute-api:Invoke"
        ],
        "Effect": "Allow",
        "Resource": "*"
      }
      
    ]
  })
}
# ----------------------------------------------------------------------
# lambda function to execute api
# ----------------------------------------------------------------------

resource "aws_lambda_function" "lambda_api_execute" {
  depends_on = [
    null_resource.ecr_image_api
  ]
  function_name = "api-execute-mldify"
  role = aws_iam_role.iam_lambda_role_api.arn
  image_uri = "${aws_ecr_repository.ecr_repository_api.repository_url}@${data.aws_ecr_image.lambda_image_api.id}"
  package_type = "Image"
  timeout       = 600
  environment {
    # We can specify the input and output buckets for the lambda function (variables are empty strings if not specified)
    variables = {
      INPUT_BUCKET = ""
      OUTPUT_BUCKET = ""
    }
  }
}

# ----------------------------------------------------------------------
# API Gateway Execution Role
# ----------------------------------------------------------------------
resource "aws_iam_role" "apigateway_execution_role" {
  name               = "${var.app_name}-apigateway-role"
  path               = "/"
  description        = "Role assigned to API Gateway"
  assume_role_policy = data.aws_iam_policy_document.apigateway_trust_policy.json
}

# ----------------------------------------------------------------------
# API Gateway Lambda Invoke Permission Policy
# ----------------------------------------------------------------------
resource "aws_iam_policy" "lambda_permissions_policy" {
  name   = "${var.app_name}-lambda-permissions"
  path   = "/"
  policy = data.aws_iam_policy_document.apigateway_lambda_policy.json
}

# ----------------------------------------------------------------------
# API Gateway Lambda Invoke Permission Policy - Attachment
# ----------------------------------------------------------------------
resource "aws_iam_role_policy_attachment" "lambda_permissions_policy_attach" {
  role       = aws_iam_role.apigateway_execution_role.name
  policy_arn = aws_iam_policy.lambda_permissions_policy.arn
}

# ----------------------------------------------------------------------
# API Gateway
# ----------------------------------------------------------------------
resource "aws_api_gateway_rest_api" "test_rest_api" {
  depends_on = [aws_cloudformation_stack.employees_api_sam_stack]
  name       = var.app_name
  endpoint_configuration {
    types = ["REGIONAL"]
  }
  body = templatefile("./files/api-def-v1.yaml", {
    app_name                   = var.app_name,
    api_gateway_execution_role = aws_iam_role.apigateway_execution_role.arn
    get_data_uri               = "${var.lambda_invoke_uri_prefix}/${data.aws_cloudformation_export.api_lambda_arn_cfn_exports["get-data"].value}/invocations"
    put_data_uri               = "${var.lambda_invoke_uri_prefix}/${data.aws_cloudformation_export.api_lambda_arn_cfn_exports["put-data"].value}/invocations"
  })
}

# ----------------------------------------------------------------------
# API Gateway Deployment
# ----------------------------------------------------------------------
resource "aws_api_gateway_deployment" "employees_rest_api_deployment" {
  depends_on  = [aws_api_gateway_rest_api.employees_rest_api]
  rest_api_id = aws_api_gateway_rest_api.employees_rest_api.id
  stage_name  = "v1"
  variables = {
    "deployed_at" = timestamp()
  }
}

# ----------------------------------------------------------------------
# API Key
# ----------------------------------------------------------------------
resource "aws_api_gateway_api_key" "api_key" {
  name        = "${var.app_name}-key"
  description = "API Key for Employees API"
}

# ----------------------------------------------------------------------
# Usage Plan
# ----------------------------------------------------------------------
resource "aws_api_gateway_usage_plan" "usage_plan" {
  name        = "${var.app_name}-usage-plan-${timestamp()}"
  description = "Usage plan for Employees"
  api_stages {
    api_id = aws_api_gateway_rest_api.employees_rest_api.id
    stage  = aws_api_gateway_deployment.employees_rest_api_deployment.stage_name
  }
}

# ----------------------------------------------------------------------
# API Key - Usage Plan Mapping
# ----------------------------------------------------------------------
resource "aws_api_gateway_usage_plan_key" "usage_plan_key" {
  key_id        = aws_api_gateway_api_key.api_key.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.usage_plan.id
}