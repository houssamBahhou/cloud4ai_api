from fastapi import APIRouter

from .endpoints import referenceUpload

router = APIRouter()
router.include_router(referenceUpload.router, prefix="/prediction", tags=["Prediction"])