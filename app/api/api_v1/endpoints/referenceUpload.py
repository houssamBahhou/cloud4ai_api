from fastapi import APIRouter, File, Form, UploadFile, HTTPException, status, Response
from loguru import logger
import boto3
import magic
from botocore.exceptions import ClientError
import time
from typing import List

DATA_ORDER = ["reference", "drift"]


MB = 1024 * 1024

SUPPORTED_FILE_TYPES = {
    'application/octet-stream': 'pkl'
}

AWS_BUCKET = "test-bucket"

s3 = boto3.resource('s3')
bucket = s3.Bucket(AWS_BUCKET)

async def s3_upload(contents: bytes, Key: str):
    logger.info(f'uploading {Key} to s3')
    bucket.put_object(Body = contents, Key = Key)

async def s3_download(key: str):
    start_time = time.time()

    while time.time() - start_time < 600:
        try:
            downloaded_file = s3.Object(bucket_name=AWS_BUCKET, key=key).get()['Body'].read()
            return downloaded_file
        except ClientError as err :
            if err.response['Error']['Code'] == "NoSuchKey":
                time.sleep(5)
            else:
                raise err
    return "Timeout: File not found in bucket after 10 minutes."

router = APIRouter()


async def upload_pickle(files: List[UploadFile], data: str):
    # check if the file is none
    if not files:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="No file found!!"
        )
    # read the content of the file
    contents = {}
    for filetype, input_ in zip(DATA_ORDER, files):
        contents[filetype] = await input_.read()

    # check file size
    output = {"files": {}}
    for file in contents:
        size = len(contents[file])
        if not 0 < size <= 10 * MB:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Supported file size is 0 - 10 MB !!"
            )
    
        # check the file type
        file_type = magic.from_buffer(buffer=contents[file], mime=True)
        if file_type not in SUPPORTED_FILE_TYPES:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Unsupported file type: {file_type}. You should upload a .pkl file"
            )

        #upload the file
        key = f"uploaded_{file}/{data}.pkl"
        await s3_upload(contents=contents[file], Key = key)

        output["files"][file] = key

    output["message"] = "File uploaded successfully!"

    return output

@router.post("/")
async def getPrediction(files: List[UploadFile], data: str = Form(...)):
    upload_response = await upload_pickle(files, data)
    if upload_response["message"] != "File uploaded successfully!":
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="File was not loaded correctly"
        )
    uploaded_files = upload_response["files"]
    print(uploaded_files)
    file_id = list(uploaded_files.values())[0].split('/')[1].split('.')[0]
    contents = await s3_download(key=f"output/{file_id}.pkl")
    if type(contents) == str:
        resp = {"error": contents}
    else:
        resp = Response(
            content=contents,
            headers={
                'Content-Disposition': f'attachement;filename={file_id}',
                'Content-Type': 'application/octet-stream'
            }
        )
    return resp

    # S3_OUTPUT = os.path.join(UPLOAD_DIR,vectorized_data)

